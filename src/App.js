import walkieTalkie from './walkie-talkie.jpg';
import styled, { keyframes } from 'styled-components';
import wobble from 'react-animations/lib/wobble'
import './App.css';

const Wobble = styled.div`animation: 5s ${keyframes`${wobble}`} infinite`;

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Wobble><img src={walkieTalkie} className={wobble} alt="logo" onClick={()=>{Talk()}} /></Wobble>
        <p>
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
        </a>
      </header>
    </div>
  );
}

function Talk() {
  console.log('hello')
}

export default App;
