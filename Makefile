build:
	docker build -t escape-room .

start:
	docker-compose up -d

bash:
	docker exec -it escape sh

stop:
	docker-compose down
